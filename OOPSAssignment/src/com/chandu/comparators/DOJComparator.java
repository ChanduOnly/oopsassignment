package com.chandu.comparators;

import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.chandu.bean.Employee;

public class DOJComparator implements Comparator<Employee>{

	@Override
	public int compare(Employee emp1, Employee emp2) {
		if(emp1.getDoj().before(emp2.getDoj()))
			return 1;
		else if (emp1.getDoj().after(emp2.getDoj()))
			return -1;
		else 
			return 0;
	}
	

}
