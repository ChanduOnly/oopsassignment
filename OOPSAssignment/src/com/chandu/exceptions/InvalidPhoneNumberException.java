package com.chandu.exceptions;

public class InvalidPhoneNumberException extends Exception{

	public void printStackTrace(){
		System.out.println("Invalid phone number. Please enter a valid phone number");
	}
	
}
