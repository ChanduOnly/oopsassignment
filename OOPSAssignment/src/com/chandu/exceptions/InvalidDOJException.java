package com.chandu.exceptions;

public class InvalidDOJException extends Exception{

	public void printStackTrace(){
		System.out.println("Invalid Date of joining. Date of joining cannot be after today.");
	}
	
}
