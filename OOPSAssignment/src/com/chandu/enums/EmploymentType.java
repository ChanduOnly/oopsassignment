package com.chandu.enums;

public enum EmploymentType {
	FULLTIME("FULLTIME"),
	CONTRACT("CONTRACT");
	
	String employmentType;

	
	private EmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}


	public String getEmploymentType() {
		return employmentType;
	}


}
