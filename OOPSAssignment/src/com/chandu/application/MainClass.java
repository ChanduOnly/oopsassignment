package com.chandu.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.chandu.bean.Address;
import com.chandu.bean.ContractEmployee;
import com.chandu.bean.Employee;
import com.chandu.bean.FullTimeEmployee;
import com.chandu.bean.Project;
import com.chandu.comparators.DOJComparator;

public abstract class MainClass {

	//Static variables
	static Map<String,Employee> employeesList = new TreeMap<String,Employee>();
	static List<Employee> allEmployees = new ArrayList<Employee>();
	static int choice = 0;
	static String values = "";
	static String[] readValues;
	static Date doj;
	static Scanner sc = new Scanner(System.in);
	static 	List<String> technologies = new ArrayList<String>();
	
	public static Date parseDate(String dOJ){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date=null;
		try {
		    //Parsing the String
		    date = dateFormat.parse(dOJ);
		} catch (ParseException e) {
		    e.printStackTrace();
		}
		return date;
	}
	public static void addEmployee(){
		technologies.add("Java");
		technologies.add("Oracle");
		technologies.add("DevOps");
		
		System.out.println("Insert Employee Details");
		System.out.println("String name, int AptNo,String cty,String state, String phoneNumber, int empId, String designation, String salary,Date(dd-MM-yyyy),String projectName,String projectDescription,String emailId,employment type(Fulltime or contract)");
		System.out.println("Enter Name,AptNo,city,state,phoneNumber,empId,designation,salary,doj,projectName,Description,emailId,employment type");
		values = sc.next();
		readValues = values.split(",");
		doj = parseDate(readValues[8]);
		if(Validations.validateDOJ(doj)&&Validations.validateEmailId(readValues[11])&&Validations.validatePhoneNumber(readValues[4])&&Validations.validateEmploymentType(readValues[12]))
		{
			if(readValues[12].toUpperCase().equals("CONTRACT")){
				employeesList.put(readValues[11],new ContractEmployee(readValues[0],new Address(readValues[1], readValues[2], readValues[3]),readValues[4],Integer.parseInt(readValues[5]),readValues[6],Integer.parseInt(readValues[7]),new Date(2012,8,8),readValues[12],new Project(readValues[9],technologies,readValues[10]),readValues[11]));
				allEmployees.add(employeesList.get(readValues[11]));
			}
			else{
				employeesList.put(readValues[11],new FullTimeEmployee(readValues[0],new Address(readValues[1], readValues[2], readValues[3]),readValues[4],Integer.parseInt(readValues[5]),readValues[6],Integer.parseInt(readValues[7]),new Date(2012,8,8),readValues[12],new Project(readValues[9],technologies,readValues[10]),readValues[11]));
				allEmployees.add(employeesList.get(readValues[11]));
			}
		}else
		{
			System.out.println("Enter correct employee details");
		}
	}
	public static void main(String[] args) {
		do{
			System.out.println("Enter your choice\n");
			System.out.println("0:If you want to exit\n1:Sort emplyees according to DOJ and display them\n2:Display Employees names having salary greater than your choice\n");
			System.out.println("3:Search all the Employees from the city of your choice\n4:Maintain a List of all employees with designation of your choice\n5:Search Employee by name\n");
			System.out.println("6:Add employee");
			choice = sc.nextInt();
			if(choice == 0)break;
			else
			{
				try{
					switch(choice)
					{
						case 1:
								Operations.sortEmployees(allEmployees);
								break;
						case 2:
								Operations.salaryComparison(allEmployees);
								break;
						case 3:
								Operations.searchByCity(allEmployees);
								break;
						case 4:
								Operations.searchByDesignation(allEmployees);
								break;
						case 5:
								Operations.searchByPartialName(allEmployees);
								break;
						case 6:
								addEmployee();
								break;
					}
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (JAXBException e) {
					e.printStackTrace();
				}
			}
		}while(choice>=1 && choice<=6);
		//chandu,1234,huntsville,alabama,9898989898,909,manager,40000,18-08-1992,devops,devopsdesc,demo@gmail.com,contract
	}

}
