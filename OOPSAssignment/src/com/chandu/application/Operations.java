package com.chandu.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.chandu.bean.Employee;
import com.chandu.comparators.DOJComparator;


public class Operations {
	static Scanner sc = new Scanner(System.in);
	public static void displayEmployees(List<Employee> allEmployees) throws SAXException, JAXBException{
		int counter = 1;
		System.out.println("List of employees according to your criteria are:\n");
		ListIterator<Employee> li = allEmployees.listIterator();
		while(li.hasNext()){
			System.out.println(counter+":"+li.next().getName());
		}
	}
	
	public static void sortEmployees(List<Employee> allEmployees) throws SAXException, JAXBException{
		List<Employee> sortedList = allEmployees;
		Collections.sort(sortedList,new DOJComparator());
		displayEmployees(sortedList);
	}
	
	public static void salaryComparison(List<Employee> allEmployees) throws SAXException, JAXBException
	{
		System.out.println("Enter a salary");
		int salary = sc.nextInt();
		List<Employee> employees = new ArrayList<Employee>();
		ListIterator<Employee> li = allEmployees.listIterator();
		while(li.hasNext()){
			Employee emp = li.next(); 
			if(emp.getSalary() == salary)
			{
				employees.add(emp);
			}
		}
		displayEmployees(employees);		
	}
	
	public static void searchByCity(List<Employee> allEmployees) throws SAXException, JAXBException
	{
		System.out.println("Enter a city name");
		String city = sc.next();
		List<Employee> employees = new ArrayList<Employee>();
		ListIterator<Employee> li = allEmployees.listIterator();
		while(li.hasNext()){
			Employee emp = li.next(); 
			if(emp.getAddress().getCity().equals(city))
			{
				employees.add(emp);
			}
		}
		displayEmployees(employees);
	}
	
	public static void searchByDesignation(List<Employee> allEmployees) throws SAXException, JAXBException
	{
		System.out.println("Enter a designation");
		String designation = sc.next();
		List<Employee> employees = new ArrayList<Employee>();
		ListIterator<Employee> li = allEmployees.listIterator();
		while(li.hasNext()){
			Employee emp = li.next(); 
			if(emp.getDesignation().equals(designation))
			{
				employees.add(emp);
			}
		}
		displayEmployees(employees);
	}
	
	public static void searchByPartialName(List<Employee> allEmployees) throws SAXException, JAXBException
	{
		System.out.println("Enter a name(Full or partial)");
		String name = sc.next();
		List<Employee> employees = new ArrayList<Employee>();
		ListIterator<Employee> li = allEmployees.listIterator();
		while(li.hasNext()){
			Employee emp = li.next(); 
			if(emp.getName().contains(name))
			{
				employees.add(emp);
			}
		}
		displayEmployees(employees);
	}
	
}
