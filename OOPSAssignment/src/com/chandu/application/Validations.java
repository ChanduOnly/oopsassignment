package com.chandu.application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.chandu.enums.EmploymentType;
import com.chandu.exceptions.InvalidDOJException;
import com.chandu.exceptions.InvalidEmailIdException;
import com.chandu.exceptions.InvalidPhoneNumberException;

public class Validations {
	private static final String EMAIL_PATTERN =
			"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static boolean validatePhoneNumber(String phoneNumber){
		Pattern pattern = Pattern.compile("\\d{10}");
		Matcher matcher = pattern.matcher(phoneNumber); 
		if (!matcher.matches()) {
		    try {
		    	throw new InvalidPhoneNumberException();
		    } catch (InvalidPhoneNumberException e) {
				e.printStackTrace();
				return false;
		    }
		 }else{
			 return true;
		 }
	}
	public static boolean validateEmailId(String emailId){
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(emailId);
		if(!matcher.matches())
		{
			try {
				throw new InvalidEmailIdException();
			} catch (InvalidEmailIdException e) {
				e.printStackTrace();
			}
			return false;
		}
		else 
		{
			return true;
		}
	}
	public static boolean validateDOJ(Date dOJ){
		if(dOJ.after(new Date()))
		{
			try {
				throw new InvalidDOJException();
			} catch (InvalidDOJException e) {
				e.printStackTrace();
			}
			return false;
		}
		else 
		{
			return true;
		}
	}
	public static boolean validateEmploymentType(String empType){
		
		if((empType.toUpperCase().equals(EmploymentType.FULLTIME.getEmploymentType()))||(empType.toUpperCase().equals(EmploymentType.CONTRACT.getEmploymentType()))){
			return true;
		}
		else
			return false;
	}
}
