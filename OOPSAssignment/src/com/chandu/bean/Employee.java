package com.chandu.bean;

import java.util.Date;

public abstract class Employee extends Person{

	private int empId;
	private String designation;
	private int salary;
	private Date doj;
	private Project project;
	private String employmentType;
	public Employee(String name, Address address, String phoneNumber, int empId, String designation, int salary,
			Date doj, String employmentType, Project project,String emailId) {
		super(name, address, phoneNumber, emailId);
		this.empId = empId;
		this.designation = designation;
		this.salary = salary;
		this.doj = doj;
		this.project = project;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public Date getDoj() {
		return doj;
	}
	public void setDoj(Date doj) {
		this.doj = doj;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	public String getEmploymentType() {
		return employmentType;
	}
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}
		
}
