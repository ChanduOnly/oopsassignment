package com.chandu.bean;

import java.util.List;

public class Project {
	private String name;
	private List<String> technologies;
	private String description;
	public Project(String name, List<String> technologies, String description) {
		super();
		this.name = name;
		this.technologies = technologies;
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getTechnologies() {
		return technologies;
	}
	public void setTechnologies(List<String> technologies) {
		this.technologies = technologies;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
