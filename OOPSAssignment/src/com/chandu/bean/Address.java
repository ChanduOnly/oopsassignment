package com.chandu.bean;

public class Address{
	private String aptNo;
	private String city;
	private String state;
	public Address(String aptNo, String city, String state) {
		super();
		this.aptNo = aptNo;
		this.city = city;
		this.state = state;
	}
	public String getAptNo() {
		return aptNo;
	}
	public void setAptNo(String aptNo) {
		this.aptNo = aptNo;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
		
}
